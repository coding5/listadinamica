#include <iostream>

using namespace std;

struct Node{
   string value;
   Node* next;
};

class Coda{
    public:
        Node* firstNode;

        Coda(){
            firstNode = new Node;
            nFullNodes = 0;
        }

        void add (string const& value) {
            if (nFullNodes == 0){   // Il primo nodo l'ho già creato nel costruttore
               firstNode->value = value;    // quindi lo riempio e basta
            }
            else{
               Node* NodeToAdd = new Node;
               Node* lastNode = getLastNode();
               lastNode->next = NodeToAdd;
               NodeToAdd->value = value;
            }
            nFullNodes++;
        }

        Node get(){
            Node nodeToReturn = *firstNode;
            firstNode = firstNode->next;
            nFullNodes--;
            return nodeToReturn;
        }

    private:
        int nFullNodes;

        Node* getLastNode(){
           Node* lastNode = firstNode;
           for(int i = 0; i < nFullNodes-1; i++){
              lastNode = lastNode->next;
           }
           return lastNode;
        }
};

int main(){
    Coda coda = Coda();

    coda.add("aa");
    coda.add("bb");
    coda.add("cc");
    cout << coda.get().value << endl;
    cout << coda.get().value << endl;
    coda.add("AA");
    coda.add("BB");
    cout << coda.get().value << endl;
    coda.add("CC");
    cout << coda.get().value << endl;
}
