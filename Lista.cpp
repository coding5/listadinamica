#include <iostream>

using namespace std;

// Bug: nFullNodes non rispecchia sempre il numero di nodi pieni nella lista
// Bug: aggiungere un elemento a una lista vuota crea un errore di segmentazione

struct Node{
   string value;
   Node* next;
};

class List{
   public:
      Node* firstNode;

      List(){
         firstNode = new Node;
         nFullNodes = 0;
      }

      Node* operator [] (int n){
         return getNodeAt(n);
      }

      void add (string const& value) {
         if (nFullNodes == 0){   // Il primo nodo l'ho già creato nel costruttore
            firstNode->value = value;
            // cout << "Aggiunta prima Nodea contenente: " << firstNode->value << " in ";
            // cout << firstNode << endl;
         }
         else{
            Node* NodeToAdd = new Node;
            Node* lastNode = getNodeAt(nFullNodes - 1);
            lastNode->next = NodeToAdd;
            NodeToAdd->value = value;
            // cout << "Aggiunta Nodea contenente: " << NodeToAdd->value << " in ";
            // cout << NodeToAdd << " connessa a " << lastNode << endl;
         }
         nFullNodes++;
      }

      void add(List list){
         getNodeAt(nFullNodes - 1)->next = list.getNodeAt(0);
         nFullNodes += list.getNFullNodes();
      }

      void pop(int n){
         if (n == 0){   // Devo eliminare la prima
            firstNode = getNodeAt(1);
         }
         else if(n == nFullNodes){  // Devo eliminare l'ultima
             Node* newLast = getNodeAt(nFullNodes-2); // Ottengo la penultima
             newLast->next = 0;  // E rimuovo l'indirizzo a cui punta
         }
         else{    // Devo eliminare una Nodea in mezzo
            Node* nextNode = getNodeAt(n+1);
            Node* prevNode = getNodeAt(n-1);
            prevNode->next = nextNode;
         }
         nFullNodes--;
      }

      void inject(string value, int n){
         Node* NodeToAdd = new Node;
         NodeToAdd->value = value;
         if(n == 0){
            Node* prevFirstNode = firstNode;
            firstNode = NodeToAdd;
            inject(prevFirstNode->value, 1);
            firstNode->next = prevFirstNode;
         }
         else{
            Node* NodeBefore = getNodeAt(n-1);
            Node* NodeAfter = getNodeAt(n);
            NodeToAdd->next = NodeAfter;
            NodeBefore->next = NodeToAdd;
            nFullNodes++;
         }
      }

      void print(){
         cout << "[";
         for(int i = 0; i < nFullNodes; i++){
            cout << getNodeAt(i)->value;
            if (i < nFullNodes - 1)
               cout << ", ";
         }
         cout << "]" << endl;
      }

      int getNFullNodes() {
         return nFullNodes;
      }

   private:
      Node* getNodeAt(int n){
         Node* lastNode = firstNode;
         for(int i = 0; i < n; i++){
            lastNode = lastNode->next;
         }
         return lastNode;
      }

      int nFullNodes;
};

int main(){
   List lista = List();
   List listaCorta = List();
   lista.add("Str1");
   lista.add("Str2");
   lista.add("Str3");
   lista.add("Str4");
   lista[1]->value = "STR333";
   lista.print();
   lista.inject("Nodo aggiunto 1", 0);
   lista.inject("Nodo aggiunto 2", 2);
   lista.print();
   listaCorta.add("abcde");
   listaCorta.add("efgh");
   listaCorta.print();
   lista.add(listaCorta);
   lista.print();
}
